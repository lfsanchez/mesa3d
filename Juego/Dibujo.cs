﻿using OpenTK.Graphics.OpenGL;
using OpenTK.Mathematics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Juego
{
    class Dibujo
    {
        float[] vertices;
        int VertexBufferObject;
        int VertexArrayObject;
        public Shader shader;
        public Texturas textura;
        Vector3 pos;
        float ancho;
        float largo;
        float alto;

        public Dibujo(Vector3 posicion, float Ancho, float Largo, float Alto)
        {
            pos = posicion;
            ancho = Ancho;
            largo = Largo;
            alto = Alto;
        }

        void Load()
        {
            shader = new Shader();
            textura = new Texturas(Properties.Resources.texturas); 
            VertexArrayObject = GL.GenVertexArray();
            GL.BindVertexArray(VertexArrayObject);
            VertexBufferObject = GL.GenBuffer();
            GL.BindBuffer(BufferTarget.ArrayBuffer, VertexBufferObject);
            GL.BufferData(BufferTarget.ArrayBuffer, vertices.Length * sizeof(float), vertices, BufferUsageHint.StaticDraw);
            GL.VertexAttribPointer(0, 4, VertexAttribPointerType.Float, true, 5 * sizeof(float), 0);
            int texCoordLocation = shader.GetAttribLocation("aTexCoord");
            GL.EnableVertexAttribArray(texCoordLocation);
            GL.VertexAttribPointer(texCoordLocation, 2, VertexAttribPointerType.Float, true, 5 * sizeof(float), 3 * sizeof(float));
            GL.EnableVertexAttribArray(0);
            shader.Use();
        }

        float[] Base(Vector3 p, float an, float lar, float alt, Vector2 textpos)
        {
            float [] vert = new float[]
            {
                //base superior
                p.X,     p.Y,     p.Z-an, textpos.X, 1.0f,
                p.X,     p.Y,     p.Z,    textpos.X, 0.0f,
                p.X+lar, p.Y,     p.Z,    textpos.Y, 0.0f,

                p.X+lar, p.Y,     p.Z,    textpos.Y, 0.0f,
                p.X,     p.Y,     p.Z-an, textpos.X, 1.0f,
                p.X+lar, p.Y,     p.Z-an, textpos.Y, 1.0f,
                //base inferior
                p.X,     p.Y-alt, p.Z-an, textpos.X, 1.0f,
                p.X,     p.Y-alt, p.Z,    textpos.X, 0.0f,
                p.X+lar, p.Y-alt, p.Z,    textpos.Y, 0.0f,

                p.X+lar, p.Y-alt, p.Z,    textpos.Y, 0.0f,
                p.X,     p.Y-alt, p.Z-an, textpos.X, 1.0f,
                p.X+lar, p.Y-alt, p.Z-an, textpos.Y, 1.0f,
                //borde frontal
                p.X,     p.Y,     p.Z,    textpos.X, 1.0f,
                p.X,     p.Y-alt, p.Z,    textpos.X, 0.0f,
                p.X+lar, p.Y,     p.Z,    textpos.Y, 1.0f,

                p.X,     p.Y-alt, p.Z,    textpos.X, 0.0f,
                p.X+lar, p.Y,     p.Z,    textpos.Y, 1.0f,
                p.X+lar, p.Y-alt, p.Z,    textpos.Y, 0.0f,
                //borde trasero
                p.X,     p.Y,     p.Z-an, textpos.X, 1.0f,
                p.X,     p.Y-alt, p.Z-an, textpos.X, 0.0f,
                p.X+lar, p.Y,     p.Z-an, textpos.Y, 1.0f,

                p.X,     p.Y-alt, p.Z-an, textpos.X, 0.0f,
                p.X+lar, p.Y,     p.Z-an, textpos.Y, 1.0f,
                p.X+lar, p.Y-alt, p.Z-an, textpos.Y, 0.0f,
                //borde lateral izquierdo
                p.X,     p.Y,     p.Z,    textpos.X, 1.0f,
                p.X,     p.Y-alt, p.Z,    textpos.X, 0.0f,
                p.X,     p.Y,     p.Z-an, textpos.Y, 1.0f,

                p.X,     p.Y,     p.Z-an, textpos.Y, 1.0f,
                p.X,     p.Y-alt, p.Z,    textpos.X, 0.0f,
                p.X,     p.Y-alt, p.Z-an, textpos.Y, 0.0f,
                //borde lateral derecho
                p.X+lar, p.Y,     p.Z,    textpos.X, 1.0f,
                p.X+lar, p.Y-alt, p.Z,    textpos.X, 0.0f,
                p.X+lar, p.Y,     p.Z-an, textpos.Y, 1.0f,

                p.X+lar, p.Y,     p.Z-an, textpos.Y, 1.0f,
                p.X+lar, p.Y-alt, p.Z,    textpos.X, 0.0f,
                p.X+lar, p.Y-alt, p.Z-an, textpos.Y, 0.0f,
            };
            return vert;
        }

        public void mesa(Vector2 textpos)
        {
            float lar;
            if (ancho > largo)
            {
                lar = ancho;
            }
            else
            {
                lar = largo;
            }
            float[] basemesa = Base(pos, ancho, largo, alto*10/100, textpos);
            float[] pata1 = Base(new Vector3(pos.X+largo*10/100,pos.Y- alto * 10 / 100, pos.Z-ancho*10/100), lar * 10 / 100, lar * 10 / 100, alto-alto * 10 / 100, textpos);
            float[] pata2 = Base(new Vector3(pos.X + largo * 10 / 100, pos.Y - alto * 10 / 100, pos.Z - ancho + ancho * 20 / 100), lar * 10 / 100, lar * 10 / 100, alto - alto * 10 / 100, textpos);
            float[] pata3 = Base(new Vector3(pos.X + largo - largo * 20 / 100, pos.Y - alto * 10 / 100, pos.Z - ancho * 10 / 100), lar * 10 / 100, lar * 10 / 100, alto - alto * 10 / 100, textpos);
            float[] pata4 = Base(new Vector3(pos.X + largo - largo * 20 / 100, pos.Y - alto * 10 / 100, pos.Z - ancho + ancho * 20 / 100), lar * 10 / 100, lar * 10 / 100, alto - alto * 10 / 100, textpos);
            vertices = basemesa.Concat(pata1).ToArray().Concat(pata2).ToArray().Concat(pata3).ToArray().Concat(pata4).ToArray();
            Load();
        }

        public void unload()
        {
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
            GL.DeleteBuffer(VertexBufferObject);
            shader.Dispose();
        }

        public void render(float width, float height)
        {
            Matrix4 view = Matrix4.CreateTranslation(pos.X, pos.Y, pos.Z);
            Matrix4 projection = Matrix4.CreatePerspectiveFieldOfView(MathHelper.DegreesToRadians(45.0f), width / height, 0.1f, 100.0f);
            Matrix4 modelX = Matrix4.CreateRotationX((float)MathHelper.DegreesToRadians(0));
            Matrix4 modelY = Matrix4.CreateRotationY((float)MathHelper.DegreesToRadians(0));
            Matrix4 modelZ = Matrix4.CreateRotationZ((float)MathHelper.DegreesToRadians(0));
            shader.SetMatrix4("modelX", modelX);
            shader.SetMatrix4("modelY", modelY);
            shader.SetMatrix4("modelZ", modelZ);
            shader.SetMatrix4("view", view);
            shader.SetMatrix4("projection", projection);
            GL.BindVertexArray(VertexArrayObject);
            GL.DrawArrays(PrimitiveType.Triangles, 0, vertices.Length);
        }

        public void render(float width, float height, double rX, double rY, double rZ, bool iX, bool iY, bool iZ)
        {
            Matrix4 view = Matrix4.CreateTranslation(pos.X, pos.Y, pos.Z);
            Matrix4 projection = Matrix4.CreatePerspectiveFieldOfView(MathHelper.DegreesToRadians(45.0f), width / height, 0.1f, 100.0f);
            Matrix4 modelX;
            Matrix4 modelY;
            Matrix4 modelZ;
            if (iX)
                modelX = Matrix4.Identity * Matrix4.CreateRotationX((float)MathHelper.DegreesToRadians(rX));
            else
                modelX =Matrix4.CreateRotationX((float)MathHelper.DegreesToRadians(rX));
            if (iY)
                modelY = Matrix4.Identity * Matrix4.CreateRotationY((float)MathHelper.DegreesToRadians(rY));
            else
                modelY = Matrix4.CreateRotationY((float)MathHelper.DegreesToRadians(rY));
            if (iZ)
                modelZ = Matrix4.Identity * Matrix4.CreateRotationZ((float)MathHelper.DegreesToRadians(rZ));
            else
                modelZ = Matrix4.CreateRotationZ((float)MathHelper.DegreesToRadians(rZ));
            shader.SetMatrix4("modelX", modelX);
            shader.SetMatrix4("modelY", modelY);
            shader.SetMatrix4("modelZ", modelZ);
            shader.SetMatrix4("view", view);
            shader.SetMatrix4("projection", projection);
            GL.BindVertexArray(VertexArrayObject);
            GL.DrawArrays(PrimitiveType.Triangles, 0, vertices.Length);
        }
        public void render(Vector3 proj, float width, float height)
        {
            Matrix4 view = Matrix4.CreateTranslation(proj.X, proj.Y, proj.Z);
            Matrix4 projection = Matrix4.CreatePerspectiveFieldOfView(MathHelper.DegreesToRadians(45.0f), width / height, 0.1f, 100.0f);
            Matrix4 modelX = Matrix4.CreateRotationX((float)MathHelper.DegreesToRadians(0));
            Matrix4 modelY = Matrix4.CreateRotationY((float)MathHelper.DegreesToRadians(0));
            Matrix4 modelZ = Matrix4.CreateRotationZ((float)MathHelper.DegreesToRadians(0));
            shader.SetMatrix4("modelX", modelX);
            shader.SetMatrix4("modelY", modelY);
            shader.SetMatrix4("modelZ", modelZ);
            shader.SetMatrix4("view", view);
            shader.SetMatrix4("projection", projection);
            GL.BindVertexArray(VertexArrayObject);
            GL.DrawArrays(PrimitiveType.Triangles, 0, vertices.Length);
        }

        public void render(Vector3 proj, float width, float height, double rX, double rY, double rZ, bool iX, bool iY, bool iZ)
        {
            Matrix4 view = Matrix4.CreateTranslation(proj.X, proj.Y, proj.Z);
            Matrix4 projection = Matrix4.CreatePerspectiveFieldOfView(MathHelper.DegreesToRadians(45.0f), width / height, 0.1f, 100.0f);
            Matrix4 modelX;
            Matrix4 modelY;
            Matrix4 modelZ;
            if (iX)
                modelX = Matrix4.Identity * Matrix4.CreateRotationX((float)MathHelper.DegreesToRadians(rX));
            else
                modelX = Matrix4.CreateRotationX((float)MathHelper.DegreesToRadians(rX));
            if (iY)
                modelY = Matrix4.Identity * Matrix4.CreateRotationY((float)MathHelper.DegreesToRadians(rY));
            else
                modelY = Matrix4.CreateRotationY((float)MathHelper.DegreesToRadians(rY));
            if (iZ)
                modelZ = Matrix4.Identity * Matrix4.CreateRotationZ((float)MathHelper.DegreesToRadians(rZ));
            else
                modelZ = Matrix4.CreateRotationZ((float)MathHelper.DegreesToRadians(rZ));
            shader.SetMatrix4("modelX", modelX);
            shader.SetMatrix4("modelY", modelY);
            shader.SetMatrix4("modelZ", modelZ);
            shader.SetMatrix4("view", view);
            shader.SetMatrix4("projection", projection);
            GL.BindVertexArray(VertexArrayObject);
            GL.DrawArrays(PrimitiveType.Triangles, 0, vertices.Length);
        }
    }
}
