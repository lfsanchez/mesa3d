﻿using OpenTK.Windowing.Common;
using OpenTK.Windowing.Desktop;
using OpenTK.Windowing.GraphicsLibraryFramework;
using System;
using System.Collections.Generic;
using System.Text;
using OpenTK.Input;
using OpenTK.Graphics.OpenGL4;
using System.Linq;
using OpenTK.Mathematics;

namespace Juego
{
    class Pantalla : GameWindow
    {
        private double time;
        Dibujo mesa1 = new Dibujo(new Vector3(-0.5f, 0.5f, 0f), 1f, 1f, 0.6f);
        Dibujo mesa2 = new Dibujo(new Vector3(-0.2f, 0.8f, -0.2f), 0.6f, 0.4f, 0.3f);
        public Pantalla(GameWindowSettings config, NativeWindowSettings nativo) : base(config, nativo) { }
        protected override void OnLoad()
        {
            base.OnLoad();
            GL.Enable(EnableCap.DepthTest);
            mesa1.mesa(new Vector2(0.0f, 0.25f));
            mesa2.mesa(new Vector2(0.25f, 0.5f));
        }
        protected override void OnUnload()
        {
            base.OnUnload();
            mesa1.unload();
            mesa2.unload();
        }
        protected override void OnRenderFrame(FrameEventArgs e)
        {
            base.OnRenderFrame(e);
            time += 30.0 * e.Time;

            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            GL.ClearColor(0.5f, 0.0f, 0.5f, 1.0f);
            mesa1.render(new Vector3(0.0f, -0.5f, -2f), Size.X, Size.Y, 5, time, 0, false, true, false);
            mesa2.render(new Vector3(0.0f, -0.5f, -2f), Size.X, Size.Y, 5, time, 0, false, true, false);

            Context.SwapBuffers();
        }
        protected override void OnUpdateFrame(FrameEventArgs e)
        {
            KeyboardState input = KeyboardState;

            if (input.IsKeyDown(Keys.Escape))
            {
                this.Close();
            }
            base.OnUpdateFrame(e);
        }
    }
}
